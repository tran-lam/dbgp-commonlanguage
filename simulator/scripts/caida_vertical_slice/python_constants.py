# Tells what list in the list of lists is a customer, provider, or peer in the
# as adjacency list.
kCustomerDict = 0
kProviderDict = 1
kPeerDict = 2

# Converted CAIDA relationship meanings
kAs1CustomerAs2 = -1
kAs1PeerAs2 = 0
