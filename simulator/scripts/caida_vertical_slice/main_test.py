import unittest
import main
import StringIO
import os


class MainTest(unittest.TestCase):
    def CleanStringIO(self, stringio_to_clean):
        """Function that cleans up stringio that was created from a raw string.
        Basically, removes the added whitespace when lining up the entries in a
        raw string for readability.
        """
        return_stringio = StringIO.StringIO()
        for line in stringio_to_clean:
            #strip whitespace from ends
            line = line.strip()
            # add back the newline
            line += '\n'
            return_stringio.write(line)
        #last char in file is newline, remove it
        return_stringio.seek(-1, os.SEEK_END)
        return_stringio.truncate()
        return return_stringio

    def testXWriteGraphToFileXOneNodeWithOneCustomerXCorrectOutput(self):
        #assert
        kInputAdjacencyList = {1: [{2 : -1}, {}, {}]}
        kCorrectStringIO = """2 1 -1 -1 2 1"""

        correct_stringio = StringIO.StringIO(kCorrectStringIO)
        correct_stringio = self.CleanStringIO(correct_stringio)

        result_stringio = StringIO.StringIO('')

        #act
        main.WriteGraphToFile(kInputAdjacencyList, result_stringio)

        #assert
        self.assertEqual(correct_stringio.getvalue(),
                         result_stringio.getvalue())

    def testXWriteGraphToFileXOneNodeWithMultCustomerXCorrectOutput(self):
        #assert
        kInputAdjacencyList = {1: [{2 : -1, 3 : -1, 4 : -1}, {}, {}]}
        kCorrectStringIO = """2 1 -1 -1 2 1
                              3 1 -1 -1 3 1
                              4 1 -1 -1 4 1"""

        correct_stringio = StringIO.StringIO(kCorrectStringIO)
        #clean the stringio of the extra space made for readabilityh
        correct_stringio = self.CleanStringIO(correct_stringio)

        result_stringio = StringIO.StringIO('')

        #act
        main.WriteGraphToFile(kInputAdjacencyList, result_stringio)

        #assert
        self.assertEqual(correct_stringio.getvalue(),
                         result_stringio.getvalue())

    def testXWriteGraphToFileXOneNodeWithMultPeersXCorrectOutput(self):
        #assert
        kInputAdjacencyList = {1: [{}, {}, {2 : -1, 3 : -1, 4 : -1}]}
        kCorrectStringIO = """2 1 0 -1 2 1
                              3 1 0 -1 3 1
                              4 1 0 -1 4 1"""

        correct_stringio = StringIO.StringIO(kCorrectStringIO)
        #clean the stringio of the extra space made for readabilityh
        correct_stringio = self.CleanStringIO(correct_stringio)

        result_stringio = StringIO.StringIO('')

        #act
        main.WriteGraphToFile(kInputAdjacencyList, result_stringio)

        #assert
        self.assertEqual(correct_stringio.getvalue(),
                         result_stringio.getvalue())

    def testXWriteGraphToFileXTwoNodeWithMultPeersAndCustomersXCorrectOutput(
            self):
        #assert
        kInputAdjacencyList = {1: [{5 : -1,6 : -1,7 : -1}, {}, {2 : -1, 3 : -1, 4 : -1}],
                               2: [{5 : -1, 8 : -1},{}, {1 : -1}]
        }
        kCorrectStringIO = """5 1 -1 -1 5 1
                              6 1 -1 -1 6 1
                              7 1 -1 -1 7 1
                              2 1 0 -1 2 1
                              3 1 0 -1 3 1
                              4 1 0 -1 4 1
                              5 2 -1 -1 5 2
                              8 2 -1 -1 8 2
                              1 2 0 -1 1 2"""

        correct_stringio = StringIO.StringIO(kCorrectStringIO)
        #clean the stringio of the extra space made for readabilityh
        correct_stringio = self.CleanStringIO(correct_stringio)

        result_stringio = StringIO.StringIO('')

        #act
        main.WriteGraphToFile(kInputAdjacencyList, result_stringio)

        #assert
        self.assertEqual(correct_stringio.getvalue(),
                         result_stringio.getvalue())


suite = unittest.TestLoader().loadTestsFromTestCase(MainTest)
runner = unittest.TextTestRunner()
runner.run(suite)
