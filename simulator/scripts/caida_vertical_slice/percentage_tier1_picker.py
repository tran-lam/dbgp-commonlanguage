from tier1_picker import Tier1Picker
import random

class PercentageTier1Picker(Tier1Picker):
    """Class that implements a Tier1Picker for picking tier1s based on a random
percentage."""

    rgenerator_seed_ = 1
    percent_to_pick_ = .1
    def __init__(self, rgenerator_seed, percent_to_pick):
        self.rgenerator_seed_ = rgenerator_seed
        self.percent_to_pick_ = percent_to_pick

    def GetTier1sForVerticalSlice(self, tier1_tuples):
        """Implementation of base class method. See class for documentation"""

        #constant for the tier1 position in the list of tuples retunred by
        #'utility_functions.GetTierOnes'
        kTier1PositionInTuple = 0

        # figure out how many tier1s to get (from percent_to_pick_) this is the
        # percent passed in to class multiplied by the total number of tier1s.
        # We round this to nearest int
        number_tier1s_to_use = int(
            round(self.percent_to_pick_ * len(tier1_tuples)))

        # get a random subset from the tier1_tuples
        random.seed(self.rgenerator_seed_)
        tier1s_for_vertical_slice = random.sample(tier1_tuples,
                                                  number_tier1s_to_use)

        #Non tupled list of tier1s for vertical slice. Used as
        #'GetValidPeersInVerticalSlice' for checking if an AS is a part of the
        #set of tier1s used for the vertical slice.
        non_tupled_tier1s = []
        for tupled_tier1s in tier1s_for_vertical_slice:
            non_tupled_tier1s.append(tupled_tier1s[kTier1PositionInTuple])
        return non_tupled_tier1s
