import python_constants
from link_cost_generator import LinkCostGenerator
from python_constants import kProviderDict
from python_constants import kCustomerDict
from python_constants import kPeerDict
import random
import logging
"""Contains utility functions for working with an inputed caida dataset in
format specified by README.
"""


def ConvertToAdjacencyList(open_caida_file):
    """Function that takes a open caida file in the format specified in the README
    and returns an adjacency list of it.

    The adjacency list is a hash table where the key is an AS that has
    relationships and the value is a list of 3 dictionaries where the first
    dictionary contains an ASes customers, the second is a dictionary of the
    ASes providers, and the third is a dictionary of the AS's peers. The value
    of these dictionaries is where one would put the latency associated with
    the link.

    For example:
    1 : [{2: 0} {3 : 2} {4: 99} ] says that AS 2 is a customer, AS 3 is a
    provider, and AS 4 is a peer of AS 1. If an AS has no relationships to an
    AS of a certain type, then the dictionary is empty. The values here are
    example latencies.

    Arguments:
       open_caida_file: An open file to the caida dataset in format defined by
       the input file in README. No check is made on its validity.

    Returns: An adjacency list in the format described above. The values for
    each of the links in the dictionary is a placeholder

    """

    #constants for the position of certain entries in a split entry of the caida file
    kRelationshipPosition = 2
    kAS1 = 0
    kAS2 = 1
    kCustomerRelationship = -1
    kPeerRelationship = 0

    return_dictionary = {}

    # For each line in the caida file, insert the relationship into the proper
    # place in the dictionary

    for line in open_caida_file:
        # strip the whitespace off the line so that spaces denote the different
        # entries in the line
        line = line.strip()
        splitline = line.split(' ')

        as1 = int(splitline[kAS1])
        as2 = int(splitline[kAS2])
        #this entry is a string, convert to int
        relationship = int(splitline[kRelationshipPosition])

        InsertRelationshipToDictionary(as1, as2, relationship,
                                       return_dictionary)

    return return_dictionary


def GetTierOnes(adjacency_dictionary):
    """Function returns a list of tuples whose first entry contains the tier 1 and
the second entry is the tier 1s it is directly connected to. A tier 1 is an AS
that has no providers.

    Arguments:
       adjacency_dictionary: The adjacency list to search for tier 1s for. It
       is in the format as described by 'ConvertToAdjacencyList'

    Returns: A list of tuples. For example (1, [2,3]) denotes that AS1 is a
    tier 1 AS and is connected to tier 1 ASes 2 and 3

    """
    return_list = []
    for as_num, neighbor_lists in adjacency_dictionary.iteritems():
        #if as_num is not a tier 1, then move on
        if not IsTier1(as_num, adjacency_dictionary):
            continue

        neighbor_tier_1s = []
        #here if as_num is a tier 1, check if it's neighbors are tier1s. This
        #will only be in Peers because customers will be by definition not tier
        #1s

        for neighbor_as in neighbor_lists[kPeerDict]:
            if IsTier1(neighbor_as, adjacency_dictionary):
                neighbor_tier_1s.append(neighbor_as)

        return_list.append((as_num, neighbor_tier_1s))

    return return_list


def GetDescendents(asnum, adjacency_dictionary, pruning_factor):
    """Given an asnumber in the adjacency_dictionary, return an adjacency list
containing the descendents of the asnumber given.

    Arguments:
       asnum : the asnumber to get the descendents format
       adjacency_dictionary: the dictionary containing the adjacency list for
       all ases in the graph in the format as defined in
       'ConvertToAdjacencyList'.
       pruning_factor (float): the percent chance an AS has to be pruned

    Returns: An adjacency list containing the subset of ASes that are
    descendents of 'asnum'

    """

    return_dictionary = {}

    #create list of Ases to get descendents of, this will be pushed on as we
    #find descendents we want to see has descendents. This is linearizing the
    #recursion
    descendents_to_find = []

    #put the first entry onto descendents to find to start the process.
    asnum_adjacent_neighbors = adjacency_dictionary[asnum]
    direct_descendents = asnum_adjacent_neighbors[
        python_constants.kCustomerDict]
    descendents_to_find = list(direct_descendents)
    #prune descendents to find
    descendents_to_find = PruneList(pruning_factor, descendents_to_find, 1)
    # descendents_to_find = direct_descendents

    #main loop finding and adding descendents to the return_dictionary
    while len(descendents_to_find) > 0:
        working_descendent = descendents_to_find.pop()
        # add its adjacency to the return_dictionary
        #if it is not in, give a blank neighbors list for the value of the entry
        working_descendent_neighbors = [{}, {}, {}]
        if working_descendent in adjacency_dictionary:
            working_descendent_neighbors = adjacency_dictionary[
                working_descendent]
        # Add it ot the return_diciontary. If it is already in the dictionary
        # for some reason, the value overwriting it should be the same as
        # already is there.
        return_dictionary[working_descendent] = working_descendent_neighbors

        # add descendents to find of this working_descendent
        working_descendent_customers = working_descendent_neighbors[
            kCustomerDict]

        working_descendent_customers = PruneList(pruning_factor, working_descendent_customers, 1)
        #add them to the list only if they don't exist yet in the list or
        #aren't already in the dictionary. This is to prevent descendent loops
        #(if they even exist)
        for customer in working_descendent_customers:
            if customer not in descendents_to_find and customer not in return_dictionary:
                descendents_to_find.append(customer)

    return return_dictionary


#untested
def PruneList(pruning_factor, a_list, rgenerator_seed):
    """ Prunes the elements from a list by some pruning factor amount.

    Arguments:
       pruning_factor(float): the percent chance an AS has to be pruned from a list
       a_list: The list to prune elements from.
       rgenerator_seed: the seed to use for the rgenerator

    Returns: A list containing the elements that survived the pruning
    """
    # compute the number of elements that should be in the sublist to return
    #1 - pruning factor because pruing_factor is the percentage we want to get
    #rid of
    sublist_size = int(
        round((1 - pruning_factor) * len(a_list)))
    random.seed(rgenerator_seed)

    return random.sample(a_list, sublist_size)


def AssignRandomLinkCosts(link_cost_generator, graph):
    """Given a graph in the format described in 'ConvertToAdjacencyList', assign
link costs based on the function implemented in 'link_cost_generator'. Costs
are assumed to be symmetric.

    Arguments:
       link_cost_generator: A class of type 'LinkCostGenerator' that implements
       'GenerateLinkCost'
       graph: The adjacency list to generate link costs for. Will be mutated.
    """

    #for each AS, assign a cost to each of link based on 'link_cost_generator'
    #only if it does't have a cost assigned yet (i.e. it is -1). If it doesn't,
    #then assign a cost and check the graph to see if there is the reverse
    #link. If there is, assign the reverse link the same cost.
    for primary_asnum, neighbor_dicts in graph.iteritems():
        for neighbor_dict in neighbor_dicts:
            for neighbor in neighbor_dict:
                # assign cost if no cost has been assigned
                if neighbor_dict[neighbor] == -1:
                    link_cost = link_cost_generator.GenerateLinkCost()
                    neighbor_dict[neighbor] = link_cost
                    reverse_link = FindReverseLink(primary_asnum, neighbor,
                                                   graph)
                    # print 'reverselink: ', reverse_link
                    # for reverse_dict in reverse_link:
                    if primary_asnum in reverse_link:
                        reverse_link[primary_asnum] = link_cost

                #private methods below here


def FindReverseLink(primarynode, secondarynode, graph):
    """Given a primary and secondary node and a graph, see if there is a reverse
link in the graph. If there is, return a reference to dictionary
that contains the reverse end of the link.

    The reverse of the link is secondarynode ---> primarynode

    Arguments:
       primarnode: The node describing one end of the link.
       secondarynode: The node describing the other end of the link
       graph: The adjacency list with the links in it

    Returns: A reference to a dictionary containing primary node. This
    describes the properties of the link 'secondarynode --> primarynode.
    Returns an empty dict if it doesn't exist.

    """
    return_dict = {}
    if secondarynode not in graph:
        return return_dict
    logger_counter = 0  #counter used for logging purposes only
    #find if the secondary node exists
    for neighbor_dict in graph[secondarynode]:
        # print 'neighbordict', neighbor_dict
        if primarynode in neighbor_dict:
            logger_counter += 1
            return_dict = neighbor_dict
    if logger_counter > 1:
        logging.debug(
            'Instance of AS that has links of two types of roles (customer and peer, provider and peer, etc): %i %i\n secondarynodedicts: %s',
            primarynode, secondarynode, graph[secondarynode])
    return return_dict


def InsertRelationshipToDictionary(as1, as2, relationship, mutable_dictionary):
    """ Function that inserts the proper entries into the table based on the relationship

    Arguments:
       as1 : customer AS (int)
       as2 : provider AS (int)
       relationship: denotes the relationship of the pair of ASes (int)
       mutable_dictionary : dictionary to mutate and add entries to
    """

    #check if dictionary has each entry in it, if it doesn't, then create one
    #for them with three empty lists.
    if as1 not in mutable_dictionary:
        mutable_dictionary[as1] = [{}, {}, {}]
    if as2 not in mutable_dictionary:
        mutable_dictionary[as2] = [{}, {}, {}]

    #lists to mutate in the rest of the function
    as1_dicts = mutable_dictionary[as1]
    as2_dicts = mutable_dictionary[as2]

    # if relationship is CustomerRelationship, then AS1 is customer of AS2,
    # and conversely, AS2 is provider of AS1
    if relationship == python_constants.kAs1CustomerAs2:
        # if As2 is not in providers for AS1, then put as2 in there
        if as2 not in as1_dicts[python_constants.kProviderDict]:
            as1_dicts[python_constants.kProviderDict].update({as2: -1})
        # if AS1 is not in customer for AS2, then put as1 in there
        if as1 not in as2_dicts[python_constants.kCustomerDict]:
            as2_dicts[python_constants.kCustomerDict].update({as1: -1})
    #if relationship is peer relationship, then AS1 is peer of as2.
    elif relationship == python_constants.kAs1PeerAs2:
        if as2 not in as1_dicts[python_constants.kPeerDict]:
            as1_dicts[python_constants.kPeerDict].update({as2: -1})
        if as1 not in as2_dicts[python_constants.kPeerDict]:
            as2_dicts[python_constants.kPeerDict].update({as1: -1})


def IsTier1(as_num, adjacency_list):
    """Returns true if the as_num is a tier 1. It verifies this by checking the
adjacency list. No arguments are mutated. No check is made with whether
'as_num' is actually in 'adjacency_list'

    Arguments:
       as_num : the as_num to see if it is a tier1 as
       adjacency_list : the adjacency list to check for this fact.

    Returns: True if as_num is indeed a tier 1 as
    """

    #get the lists containing the neighbors of as_num
    neighbors_list = adjacency_list[as_num]

    if len(neighbors_list[kProviderDict]) > 0:
        return False
    else:
        return True
