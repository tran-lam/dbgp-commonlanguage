import unittest
import random
from link_cost_generator import LinkCostGenerator
import utility_functions
import StringIO
import copy


class UtilityFunctionsTest(unittest.TestCase):
    def testXConvertToAdjacencyListXInputFileWith1CustomerEntryXGetCorrectAdjacencyList(
            self):
        #arrange
        kInputCaida = """1 2 -1"""

        kCorrectDictionary = {1: [{}, {2: -1}, {}], 2: [{1: -1}, {}, {}]}

        stringio_input = StringIO.StringIO(kInputCaida)

        #act
        result_dict = utility_functions.ConvertToAdjacencyList(stringio_input)

        #assert
        self.assertDictEqual(kCorrectDictionary, result_dict)

    def testXConvertToAdjacencyListXInputFileWith1PeerEntryXGetCorrectAdjacencyList(
            self):
        #arrange
        kInputCaida = """1 2 0"""

        kCorrectDictionary = {1: [{}, {}, {2: -1, }], 2: [{}, {}, {1: -1, }]}

        stringio_input = StringIO.StringIO(kInputCaida)

        #act
        result_dict = utility_functions.ConvertToAdjacencyList(stringio_input)

        #assert
        self.assertDictEqual(kCorrectDictionary, result_dict)

    def testXConvertToAdjacencyListXInputFileWith2EntryBothTypesXGetCorrectAdjacencyList(
            self):
        #arrange
        kInputCaida = """1 2 0
                         1 2 -1"""

        kCorrectDictionary = {1: [{}, {2: -1, }, {2: -1, }],
                              2: [{1: -1, }, {}, {1: -1, }]}

        stringio_input = StringIO.StringIO(kInputCaida)

        #act
        result_dict = utility_functions.ConvertToAdjacencyList(stringio_input)

        #assert
        self.assertDictEqual(kCorrectDictionary, result_dict)

    def testXConvertToAdjacencyListXInputFileWith2DupPeerEntryXGetCorrectAdjacencyList(
            self):
        #arrange
        kInputCaida = """1 2 0
                         2 1 0"""

        kCorrectDictionary = {1: [{}, {}, {2: -1, }], 2: [{}, {}, {1: -1, }]}

        stringio_input = StringIO.StringIO(kInputCaida)

        #act
        result_dict = utility_functions.ConvertToAdjacencyList(stringio_input)

        #assert
        self.assertDictEqual(kCorrectDictionary, result_dict)

    def testXConvertToAdjacencyListXInputFileWithMultEntryXGetCorrectAdjacencyList(
            self):
        #arrange
        kInputCaida = """1 2 0
                         2 1 0
                         3 4 -1
                         3 2 0
                         1 3 -1
                         4 2 0"""

        kCorrectDictionary = {1: [{}, {3: -1, }, {2: -1, }],
                              2: [{}, {}, {1: -1,
                                           3: -1,
                                           4: -1, }],
                              3: [{1: -1, }, {4: -1, }, {2: -1, }],
                              4: [{3: -1, }, {}, {2: -1, }]}

        stringio_input = StringIO.StringIO(kInputCaida)

        #act
        result_dict = utility_functions.ConvertToAdjacencyList(stringio_input)

        #assert
        self.assertDictEqual(kCorrectDictionary, result_dict)

    def testXGetTierOnesXInputAdjacencyListWith1Teir1XGetListWith1Tier1(self):
        #arrange
        kInputAdjacencyList = {1: [{2: -1, }, {}, {}]}

        kCorrectTupleList = [(1, [])]

        #act
        result_tuples = utility_functions.GetTierOnes(kInputAdjacencyList)

        #assert
        self.assertListEqual(kCorrectTupleList, result_tuples)

    def testXGetTierOnesXInputAdjacencyListWith2Teir1XGetCorrectTier1s(self):
        #arrange
        kInputAdjacencyList = {
            1: [{2: -1, }, {}, {}],
            3: [{4: -1, }, {}, {}],
        }

        kCorrectTupleList = [(1, []), (3, [])]

        #act
        result_tuples = utility_functions.GetTierOnes(kInputAdjacencyList)

        #assert
        self.assertListEqual(kCorrectTupleList, result_tuples)

    def testXGetTierOnesXInputAdjacencyListWith2Tier1TheyAreNeighborsXGetCorrectTier1s(
            self):
        #arrange
        kInputAdjacencyList = {
            1: [{2: -1, }, {}, {3: -1, }],
            3: [{4: -1, }, {}, {1: -1, }],
        }

        kCorrectTupleList = [(1, [3]), (3, [1])]

        #act
        result_tuples = utility_functions.GetTierOnes(kInputAdjacencyList)

        #assert
        self.assertListEqual(kCorrectTupleList, result_tuples)

    def testXGetTierOnesXInputAdjacencyListWith3Tier1TwoAreNeighborsOneIsntXGetCorrectTier1s(
            self):
        #arrange
        kInputAdjacencyList = {
            1: [{2: -1, }, {}, {3: -1, }],
            3: [{4: -1, }, {}, {1: -1, }],
            4: [{4: -1, }, {}, {}],
        }

        kCorrectTupleList = [(1, [3]), (3, [1]), (4, [])]

        #act
        result_tuples = utility_functions.GetTierOnes(kInputAdjacencyList)

        #assert
        self.assertListEqual(kCorrectTupleList, result_tuples)

    def testXGetTierOnesXInputAdjacencyListWith2Tier1TheyAreNeighborsXGetListWith1Tier1(
            self):
        #arrange
        kInputAdjacencyList = {
            1: [{2: -1, }, {}, {3: -1, }],
            3: [{4: -1, }, {}, {1: -1, }],
            4: [{4: -1, }, {}, {}],
        }

        kCorrectTupleList = [(1, [3]), (3, [1]), (4, [])]

        #act
        result_tuples = utility_functions.GetTierOnes(kInputAdjacencyList)

        #assert
        self.assertListEqual(kCorrectTupleList, result_tuples)

    def testXGetTierOnesXInputAdjacencyListWithOneTier1OneNonTier1XGetListWith1Tier1(
            self):
        #arrange
        kInputAdjacencyList = {
            1: [{2: -1, }, {}, {3: -1, }],
            3: [{4: -1, }, {5: -1, }, {1: -1, }],
        }

        kCorrectTupleList = [(1, [])]

        #act
        result_tuples = utility_functions.GetTierOnes(kInputAdjacencyList)

        #assert
        self.assertListEqual(kCorrectTupleList, result_tuples)

    def testXGetDescendentsXGivenASWithNoDescendentsXGetEmptyDictBack(self):
        #arrange
        kInputAsNum = 1
        kInputAdjacencyList = {1: [{}, {}, {}]}

        kCorrectOutput = {}

        input_dict = copy.deepcopy(kInputAdjacencyList)
        #act
        result_dictionary = utility_functions.GetDescendents(kInputAsNum,
                                                             input_dict,0)

        #assert
        self.assertDictEqual(kCorrectOutput, result_dictionary)
        self.assertDictEqual(input_dict, kInputAdjacencyList)

    def testXGetDescendentsXGivenASWithOneDescendentXGetCorrectDictBack(self):
        #arrange
        kInputAsNum = 1
        kInputAdjacencyList = {1: [{2: -1, }, {}, {}], 2: [{}, {1: -1, }, {}]}

        kCorrectOutput = {2: [{}, {1: -1, }, {}]}

        input_dict = copy.deepcopy(kInputAdjacencyList)
        #act
        result_dictionary = utility_functions.GetDescendents(kInputAsNum,
                                                             input_dict,0)

        #assert
        self.assertDictEqual(kCorrectOutput, result_dictionary)
        self.assertDictEqual(input_dict, kInputAdjacencyList)

    def testXGetDescendentsXGivenASWithOneDescendentWithNoAdjacencyEntryXGetCorrectDictBack(
            self):
        #arrange
        kInputAsNum = 1
        kInputAdjacencyList = {1: [{2: -1, }, {}, {}]}

        kCorrectOutput = {2: [{}, {}, {}]}

        input_dict = copy.deepcopy(kInputAdjacencyList)
        #act
        result_dictionary = utility_functions.GetDescendents(kInputAsNum,
                                                             input_dict,0)

        #assert
        self.assertDictEqual(kCorrectOutput, result_dictionary)
        self.assertDictEqual(input_dict, kInputAdjacencyList)

    def testXGetDescendentsXGivenASWithDescendentThatHasDescendentXGetCorrectDictBack(
            self):
        #arrange
        kInputAsNum = 1
        kInputAdjacencyList = {1: [{2: -1, }, {}, {}],
                               2: [{3: -1, }, {1: -1, }, {}]}

        kCorrectOutput = {2: [{3: -1, }, {1: -1, }, {}], 3: [{}, {}, {}]}

        input_dict = copy.deepcopy(kInputAdjacencyList)
        #act
        result_dictionary = utility_functions.GetDescendents(kInputAsNum,
                                                             input_dict,0)

        #assert
        self.assertDictEqual(kCorrectOutput, result_dictionary)
        self.assertDictEqual(input_dict, kInputAdjacencyList)

    def testXGetDescendentsXAsWithDescendentLoopXGetCorrectDictBack(self):
        #arrange
        kInputAsNum = 1
        kInputAdjacencyList = {
            1: [{2: -1, }, {}, {}],
            2: [{3: -1, }, {1: -1, }, {}],
            3: [{4: -1, }, {2: -1, }, {}],
            4: [{2: -1, }, {3: -1, }, {}],
        }

        kCorrectOutput = {
            2: [{3: -1, }, {1: -1, }, {}],
            3: [{4: -1, }, {2: -1, }, {}],
            4: [{2: -1, }, {3: -1, }, {}],
        }

        input_dict = copy.deepcopy(kInputAdjacencyList)
        #act
        result_dictionary = utility_functions.GetDescendents(kInputAsNum,
                                                             input_dict,0)

        #assert
        self.assertDictEqual(kCorrectOutput, result_dictionary)
        self.assertDictEqual(input_dict, kInputAdjacencyList)

    class StaticLinkCostGenerator(LinkCostGenerator):
        def GenerateLinkCost(self):
            return 2.00
    class RandomLinkCostGenerator(LinkCostGenerator):
        def GenerateLinkCost(self):
            return random.uniform(1, 1000)

    def testXAssignRandomLinkCostsXGivenAdjacencyListAndStaticLinkCostGeneratorXVerifyGraphCosts(
            self):
        #arrange
        kInputAdjacencyList = {
            1: [{2: -1, }, {}, {}],
            2: [{3: -1, }, {1: -1, }, {}],
            3: [{4: -1, }, {2: -1, }, {}],
            4: [{2: -1, }, {3: -1, }, {}],
        }

        kCorrectOutput = {
            1: [{2: 2, }, {}, {}],
            2: [{3: 2, }, {1: 2, }, {}],
            3: [{4: 2, }, {2: 2, }, {}],
            4: [{2: 2, }, {3: 2, }, {}],
        }

        #act
        utility_functions.AssignRandomLinkCosts(self.StaticLinkCostGenerator(),
                                                kInputAdjacencyList)

        #assert
        self.assertDictEqual(kCorrectOutput, kInputAdjacencyList)

    def testXAssignRandomLinkCostsXGivenAdjacencyListAndRandomLinkCostGeneratorWithSymmetricLinksXVerifyGraphCosts(
            self):
        #arrange
        kInputAdjacencyList = {
            1: [{2: -1, }, {}, {}],
            2: [{}, {1: -1, }, {}],
        }

        #act
        utility_functions.AssignRandomLinkCosts(self.RandomLinkCostGenerator(),
                                                kInputAdjacencyList)

        #assert
        self.assertEqual(kInputAdjacencyList[1][0][2], kInputAdjacencyList[2][1][1])

    def testXAssignRandomLinkCostsXGivenAdjacencyListAndRandomLinkCostGeneratorWithNoSymmetricLinksXVerifyGraphCosts(
            self):
        #arrange
        kInputAdjacencyList = {
            1: [{2: -1, }, {}, {}],
        }

        #act
        utility_functions.AssignRandomLinkCosts(self.RandomLinkCostGenerator(),
                                                kInputAdjacencyList)

        #assert
        self.assertFalse(kInputAdjacencyList[1][0][2] == -1)
suite = unittest.TestLoader().loadTestsFromTestCase(UtilityFunctionsTest)
runner = unittest.TextTestRunner()
runner.run(suite)
