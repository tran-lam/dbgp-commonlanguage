from tier1_picker import Tier1Picker
import copy
import random


class RawTier1Picker(Tier1Picker):
    """Tier1 picker that picks a raw number of tier1s (contiguous if possible). The
starting point of picking tier1s is random. There is the option of choosing a
specific starting point.

    """

    # The seed for the rgenerator (set before picking)
    rgenerator_seed_ = 1
    # The raw number of tier1s to pick
    number_to_pick_ = 4
    specific_starting_pt_ = None

    def __init__(self, rgenerator_seed, number_to_pick, specific_starting_pt=None):
        self.rgenerator_seed_ = rgenerator_seed
        self.number_to_pick_ = number_to_pick
        self.specific_starting_pt_ = specific_starting_pt

    def GetTier1sForVerticalSlice(self, tier1_tuples):
        """Implementation of base class method. See class for documentation.

        Important. This class assumes that there exists a set of contiguous
        ases of the number given.

        Returns: An empty list if there does not exist a contiguous set of ASes
        """

        random.seed(self.rgenerator_seed_)

        #If a contiguous number of ases does not exist at that spot, then do
        #this until all options have been tried. Returns an empty list on failure
        tuples_to_try = copy.deepcopy(tier1_tuples)

        # if 'specific_starting_pt_' is None, then that means we want to choose
        # a random point. Otherwise, we choose a specific point and try to get
        # a contiguous set from it.
        if self.specific_starting_pt_ == None:
            while len(tuples_to_try) > 0:
                # Find the starting point, this is where the randomness comes in
                # Element to choose in format starting at 1 to the length of items.
                # Therefore, when getting the starting point from the list, subtract
                # this by 1.
                element_to_choose = random.randint(1, len(tuples_to_try))
                starting_tier1 = tuples_to_try.pop(element_to_choose - 1)[0]
                contiguous_set = self.GetContiguousSet(tier1_tuples, starting_tier1)
                if len(contiguous_set) > 0:
                    return contiguous_set
        else:
            contiguous_set = self.GetContiguousSet(tier1_tuples, self.specific_starting_pt_)
            if len(contiguous_set) > 0:
                return contiguous_set

        #Here if there was no contiguous set that existed.
        return []

    #private methods here

    def GetContiguousSet(self, tier1_tuples, starting_tier1):
        """Helper function that gets a raw number of contiguous tier1s back based on
'number_to_pick'

        Arguments:
           tier1_tuples: The set of tier1s tuples.
           starting_tier1: The tier 1 we have determined to start at.

        Returns:
           True: if it was successful in finding a contiguous set
           and
           A contiguous list of ases
        """

        #create an adjacency list from these tuples
        tier1_to_neighbors = {}
        for tier1_tuple in tier1_tuples:
            tier1 = tier1_tuple[0]
            tier1_to_neighbors[tier1] = tier1_tuple[1]

        #check if 'starting_tier1' exists in the set. This is because, someone
        #could give a starting tier1 that doesn't exist. If it is not there,
        #return empty list.
        if starting_tier1 not in tier1_to_neighbors:
            return []

        #get a contiguous set of these by doing a search. The nodes that are
        #contiguous will be in 'nodes_visited' so return that when it reaches
        #the number we want.
        nodes_to_lookat = []
        nodes_visited = []
        nodes_to_lookat.append(starting_tier1)
        while len(nodes_to_lookat) > 0:
            working_node = nodes_to_lookat.pop()
            if working_node not in nodes_visited:
                nodes_visited.append(working_node)
                if len(nodes_visited) == self.number_to_pick_:
                    return nodes_visited
            working_node_neighbors = tier1_to_neighbors[working_node]
            for neighbor in working_node_neighbors:
                if neighbor not in nodes_to_lookat and neighbor not in nodes_visited:
                    nodes_to_lookat.append(neighbor)
        #if here, it is possible that we don't have all the nodes that we need.
        #Check.
        if len(nodes_visited) != self.number_to_pick_:
            return []
        else:
            return nodes_visited
