Table of Contents
_________________

1 Overview
2 Commandline arguments
.. '-i:
.. '-o'
.. '-P'
..... percentage
..... raw
.. '-p'
.. '-r' '--raw'
.. '-d' '--depth
.. '-s'
.. '-S', '--starting_pt'


1 Overview
==========

  Python program for generating a vertical slice from the CAIDA dataset.

  A vertical slice consists of the descendents of a tier 1 AS (an as
  that has no providers). It also includes peers of decendents if the
  peer is a decendent of a tier 1 AS that is a part of the vertical
  slice we are generating. There can be multiple tier 1 ASes selected
  for a vertical slice.


2 Commandline arguments
=======================

'-i:
~~~~

  File that contains the relationship data from CAIDA
  - Note: Caida data should be run through script
    'convert_caida_dataset.awk' as we reverse the provider and customer
    notation (caida has provider -> customer as -1, we have customer ->
    provider as -1).


'-o'
~~~~

  output file location (path should end with file name)


'-P'
~~~~

  The type of tier1 picker to use. Options are 'percentage' and 'raw'.


percentage
----------

  picks a random percent of tier1s to create the vertical slice from.
  Picks uniformly without regard to neighbors.


raw
---

  picks a set raw number of tier 1s (contiguous if possible). The
  starting point of picking tier1s is random. If a contiguous number
  does not exist, then nothing happens.


'-p'
~~~~

  Percentage of tier 1's to include in virtical slice (default .1)
    Required for doing the percentage tier1 picker


'-r' '--raw'
~~~~~~~~~~~~

  The raw number of tier1s to pick. This goes along with the '-P "raw"'
  option. Default is 4.


'-d' '--depth
~~~~~~~~~~~~~

  The depth limit for the vertical slice. For example, if set to '2',
  then the vertical slice will contain exclusvily nodes less than or
  equal to two hops from the tiers. Default is 9999.


'-s'
~~~~

  Random number generator seed when picking random tier 1's to include
    in slice


'-S', '--starting_pt'
~~~~~~~~~~~~~~~~~~~~~

  A specific tier1 to start from. This is paired with the "raw" picker
