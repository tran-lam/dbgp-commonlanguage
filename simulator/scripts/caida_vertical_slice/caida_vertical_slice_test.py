import unittest
from caida_vertical_slice import CaidaVerticalSlice
from percentage_tier1_picker import PercentageTier1Picker
from raw_tier1_picker import RawTier1Picker
import StringIO

class CaidaVerticalSliceTest(unittest.TestCase):
    def testXGetVerticalSliceXOneTier1OneDescendentXGetCorrectSlice(self):
        #arrange
        kInputCaidaFile = """1 2 -1"""
        kPercentTier1s = 1
        kRgeneratorSeed = 1

        kCorrectOutput = { 1 : [{},{2 : -1},{}],
                           2 : [{1 : -1},{},{}]
        }

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(PercentageTier1Picker(kRgeneratorSeed, kPercentTier1s))

        #assert
        self.assertDictEqual(result_vertical_slice, kCorrectOutput)

    # had to do some trickery to get it so that the peer is not in the slice,
    # that is why rgenerator is set to .5
    def testXGetVerticalSliceXTwoTier1OneDescendentHasPeerXGetCorrectSlice(self):
        #arrange
        kInputCaidaFile = """1 2 -1
                             3 4 -1
                             1 3 0"""
        kPercentTier1s = .5
        kRgeneratorSeed = 1

        kCorrectOutput = { 1 : [{},{2 : -1},{}],
                           2 : [{1 : -1},{},{}]
        }

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(PercentageTier1Picker(kRgeneratorSeed, kPercentTier1s))

        #assert
        self.assertDictEqual(result_vertical_slice, kCorrectOutput)

    def testXGetVerticalSliceXTwoTier1MultipleDescendentsXGetCorrectSlice(self):
        #arrange
        kInputCaidaFile = """1 2 -1
                             3 4 -1
                             5 3 -1"""
        kPercentTier1s = 1
        kRgeneratorSeed = 1

        kCorrectOutput = { 1 : [{},{2 : -1},{}],
                           2 : [{1 : -1},{},{}],
                           3 : [{5 : -1},{4 : -1},{}],
                           4 : [{3 : -1},{},{}],
                           5 : [{},{3 : -1},{}],
        }

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(PercentageTier1Picker(kRgeneratorSeed, kPercentTier1s))

        #assert
        self.assertDictEqual(result_vertical_slice, kCorrectOutput)

    def testXGetVerticalSliceXTwoTier1MultipleDescendentsTheyAreNeighborsXGetCorrectSlice(self):
        #arrange
        kInputCaidaFile = """1 2 -1
                             2 4 0
                             3 4 -1
                             5 3 -1"""
        kPercentTier1s = 1
        kRgeneratorSeed = 1

        kCorrectOutput = { 1 : [{},{2 : -1},{}],
                           2 : [{1 : -1},{},{4 : -1}],
                           3 : [{5 : -1},{4 : -1},{}],
                           4 : [{3 : -1},{},{2 : -1}],
                           5 : [{},{3 : -1},{}],
        }

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(PercentageTier1Picker(kRgeneratorSeed, kPercentTier1s))

        #assert
        self.assertDictEqual(result_vertical_slice, kCorrectOutput)

    def testXGetVerticalSliceXTwoTier1MultipleDescendentsWithMaxDepth2XGetCorrectSlice(self):
        #arrange
        kInputCaidaFile = """1 2 -1
                             3 1 -1
                             4 3 -1"""
        kRawTier1s = 1
        kRgeneratorSeed = 1
        kMaxDepth = 2

        kCorrectOutput = {1 : [{3 : -1},{2 : -1},{}],
                          2 : [{1: -1},{},{}],
                          3 : [{}, {1: -1}, {}]
        }

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(RawTier1Picker(kRgeneratorSeed, kRawTier1s), kMaxDepth)

        #assert
        self.assertDictEqual(result_vertical_slice, kCorrectOutput)

    def testXGetVerticalSliceXTwoTier1MultipleDescendentsWithPeerInsideMaxDepthXGetCorrectSlice(self):
        #arrange
        kInputCaidaFile = """1 2 -1
                             3 1 -1
                             3 2 0
                             4 3 -1"""
        kRawTier1s = 1
        kRgeneratorSeed = 1
        kMaxDepth = 2

        kCorrectOutput = {1 : [{3 : -1},{2 : -1},{}],
                          2 : [{1: -1},{},{3 : -1}],
                          3 : [{}, {1: -1}, {2 : -1}]
        }

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(RawTier1Picker(kRgeneratorSeed, kRawTier1s), kMaxDepth)


        #assert
        self.assertDictEqual(result_vertical_slice, kCorrectOutput)

    def testXGetVerticalSliceXTwoTier1MultipleDescendentsWithPeerOutsideDepthXGetCorrectSlice(self):
        #arrange
        kInputCaidaFile = """1 2 -1
                             3 1 -1
                             3 5 0
                             4 3 -1
                             5 4 -1"""
        kRawTier1s = 1
        kRgeneratorSeed = 1
        kMaxDepth = 2

        kCorrectOutput = {1 : [{3 : -1},{2 : -1},{}],
                          2 : [{1: -1},{},{}],
                          3 : [{}, {1: -1}, {}]
        }

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(RawTier1Picker(kRgeneratorSeed, kRawTier1s), kMaxDepth)

        #assert
        self.assertDictEqual(result_vertical_slice, kCorrectOutput)

    def testXGetVerticalSliceX50Tier1s0PercentXOutputSize0(self):
        #arrange
        kInputCaidaFile = """1 2 -1
                             1 3 -1
                             1 4 -1
                             1 5 -1
                             1 6 -1
                             1 7 -1
                             1 8 -1
                             1 9 -1
                             1 10 -1
                             1 11 -1
                             1 12 -1
                             1 13 -1
                             1 14 -1
                             1 15 -1
                             1 16 -1
                             1 17 -1
                             1 18 -1
                             1 19 -1
                             1 20 -1
                             1 21 -1
                             1 22 -1
                             1 23 -1
                             1 24 -1
                             1 25 -1
                             1 26 -1
                             1 30 -1
                             1 31 -1
                             1 32 -1
                             1 33 -1
                             1 34 -1
                             1 35 -1
                             1 36 -1
                             1 37 -1
                             1 38 -1
                             1 39 -1
                             1 40 -1
                             1 41 -1
                             1 42 -1
                             1 43 -1
                             1 44 -1
                             1 45 -1
                             1 46 -1
                             1 47 -1
                             1 48 -1
                             1 49 -1
                             1 50 -1
                             1 51 -1
                             1 52 -1"""
        kPercentTier1s = 0
        kRgeneratorSeed = 1

        kCorrectOutputSize = 0

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(PercentageTier1Picker(kRgeneratorSeed, kPercentTier1s))

        #assert
        self.assertEqual(len(result_vertical_slice), kCorrectOutputSize)

    def testXGetVerticalSliceX50Tier1s51PercentXOutputSize27(self):
        #arrange
        kInputCaidaFile = """1 2 -1
                             1 3 -1
                             1 4 -1
                             1 5 -1
                             1 6 -1
                             1 7 -1
                             1 8 -1
                             1 9 -1
                             1 10 -1
                             1 11 -1
                             1 12 -1
                             1 13 -1
                             1 14 -1
                             1 15 -1
                             1 16 -1
                             1 17 -1
                             1 18 -1
                             1 19 -1
                             1 20 -1
                             1 21 -1
                             1 22 -1
                             1 23 -1
                             1 24 -1
                             1 25 -1
                             1 26 -1
                             1 27 -1
                             1 28 -1
                             1 29 -1
                             1 30 -1
                             1 31 -1
                             1 32 -1
                             1 33 -1
                             1 34 -1
                             1 35 -1
                             1 36 -1
                             1 37 -1
                             1 38 -1
                             1 39 -1
                             1 40 -1
                             1 41 -1
                             1 42 -1
                             1 43 -1
                             1 44 -1
                             1 45 -1
                             1 46 -1
                             1 47 -1
                             1 48 -1
                             1 49 -1
                             1 50 -1
                             1 51 -1"""
        kPercentTier1s = .51
        kRgeneratorSeed = 1

        kCorrectOutputSize = 27

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(PercentageTier1Picker(kRgeneratorSeed, kPercentTier1s))

        #assert
        self.assertEqual(len(result_vertical_slice), kCorrectOutputSize)

    def testXGetVerticalSliceX50Tier1s100PercentXOutputSize51(self):
        #arrange
        kInputCaidaFile = """1 2 -1
                             1 3 -1
                             1 4 -1
                             1 5 -1
                             1 6 -1
                             1 7 -1
                             1 8 -1
                             1 9 -1
                             1 10 -1
                             1 11 -1
                             1 12 -1
                             1 13 -1
                             1 14 -1
                             1 15 -1
                             1 16 -1
                             1 17 -1
                             1 18 -1
                             1 19 -1
                             1 20 -1
                             1 21 -1
                             1 22 -1
                             1 23 -1
                             1 24 -1
                             1 25 -1
                             1 26 -1
                             1 27 -1
                             1 28 -1
                             1 29 -1
                             1 30 -1
                             1 31 -1
                             1 32 -1
                             1 33 -1
                             1 34 -1
                             1 35 -1
                             1 36 -1
                             1 37 -1
                             1 38 -1
                             1 39 -1
                             1 40 -1
                             1 41 -1
                             1 42 -1
                             1 43 -1
                             1 44 -1
                             1 45 -1
                             1 46 -1
                             1 47 -1
                             1 48 -1
                             1 49 -1
                             1 50 -1
                             1 51 -1"""
        kPercentTier1s = 1
        kRgeneratorSeed = 1

        kCorrectOutputSize = 51

        stringio_input = StringIO.StringIO(kInputCaidaFile)

        caida_vertical_slice = CaidaVerticalSlice(stringio_input)

        #act
        result_vertical_slice = caida_vertical_slice.GetVerticalSlice(PercentageTier1Picker(kRgeneratorSeed, kPercentTier1s))

        #assert
        self.assertEqual(len(result_vertical_slice), kCorrectOutputSize)

suite = unittest.TestLoader().loadTestsFromTestCase(CaidaVerticalSliceTest)
runner = unittest.TextTestRunner()
runner.run(suite)
