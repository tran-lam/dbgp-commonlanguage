from link_cost_generator import LinkCostGenerator
import random

class UniformRandomLinkCostGenerator(LinkCostGenerator):
    """Implementation of abstract class LinkCostGenerator that generates link costs
as a uniform random number between the range passed into the class.
    """

    # the range to generate random numbers from
    lowerbound_ = 10
    upperbound_ = 1000
    

    def __init__(self, lowerbound, upperbound):
        self.lowerbound_ =  lowerbound
        self.upperbound_ = upperbound

    def GenerateLinkCost(self):
        """Implementation of abstract method in base class"""
        return random.uniform(self.lowerbound_, self.upperbound_)

if __name__ == "__main__":
    test = UniformRandomLinkCostGenerator(10, 1000)
    for i in range(0,10):
        print test.GenerateLinkCost()


