from caida_vertical_slice import CaidaVerticalSlice
from uniform_random_link_cost_generator import UniformRandomLinkCostGenerator
from percentage_tier1_picker import PercentageTier1Picker
from raw_tier1_picker import RawTier1Picker
import utility_functions
import random
import os
import argparse
from python_constants import kCustomerDict
from python_constants import kPeerDict
from python_constants import kAs1CustomerAs2
from python_constants import kAs1PeerAs2
import logging

kDoStraightPercentage = 'percentage'
kDoRawNumber = 'raw'


def InitializeArgParser():
    """ Function where you should add additional command line arguments.

    Returns: the return of argparse.parse_args()"""

    parser = argparse.ArgumentParser()
    #add argument for the configuration file
    parser.add_argument(
        '-i',
        '--input_converted_caida',
        help='File that contains the relationship data from CAIDA. The caida data should be ran through "convert_caida_dataset.awk"')
    parser.add_argument(
        '-o',
        '--output_file',
        help='output file location (path should end with file name)')
    parser.add_argument(
        '-P',
        '--picker',
        default='percentage',
        help='The type of tier1 picker to use, there is (raw and striaght percentage) see readme')
    parser.add_argument(
        '-p',
        '--percentage_tier1s',
        default=.1,
        help='percentage of tier 1s to include in vertical slice (default .1). Required for doing the Percentage tier1 picker')
    parser.add_argument(
        '-s',
        '--seed',
        default=1,
        help='randm number generator seed when picking random tier 1s to include in slice')
    parser.add_argument(
        '-r',
        '--raw',
        default=4,
        help='The raw number of tier1s to pick. This goes along with the "-P raw" option. Default is 4')
    parser.add_argument(
        '-d',
        '--depth',
        default=9999,
        help='The depth limit for the vertical slice')
    parser.add_argument(
        '-S',
        '--starting_pt',
        default=None,
        help='A specific tier1 to start from. This is paired with the "raw" picker ')
    parser.add_argument(
        '--prune_factor', default=0, help='Percentage of descendents to prune on each level')


    return parser.parse_args()


def RandomizeLinkCosts(graph):
    """This is just a testing function for setting link costs"""

    for primary_node, node_neighbors in graph.iteritems():
        for neighbor_dict in node_neighbors:
            for neighbor, link_cost in neighbor_dict.iteritems():
                neighbor_dict[neighbor] = random.randfloat(10, 1000)


def WriteGraphToFile(graph, open_file_handle):
    """Function writes the contents of an adjacency list to the file passed in.

    Each line of output of the file is in format:
    <as1> <as2> <relationshiptype> <link cost> <pop_as1> <pop_as2>

    Arguments:
       graph: The adjacency list in format {<nodenumber>: [{<customernodes>},
       {<providernodes>}, {<peernodes>}]}
       open_file_handle: the open file to write the data to. Function assumes
       that this file is open
    """
    logging.debug('Total nodes in graph being written to file: %i', len(graph))

    # go through each node and for their customers and peers, add write an
    # entry to the file in caida format. Can skip providernodes as they will be
    # automatically taken care of, and this keeps with the idea that the
    # converted caida set doesn't have entries for provider nodes.
    for primary_node, primary_node_neighbors in graph.iteritems():
        #write ine for each customer of primary node
        for customer, customer_link_cost in sorted(primary_node_neighbors[
                kCustomerDict].iteritems()):
            output_line = str(customer) + ' ' + str(primary_node) + ' ' + str(
                kAs1CustomerAs2) + ' ' + str(customer_link_cost) + ' ' + str(
                    customer) + ' ' + str(primary_node) + '\n'
            open_file_handle.write(output_line)

        #write line for each peer of primary node (repeates should be fine)
        for peer, peer_link_cost in sorted(primary_node_neighbors[kPeerDict]
                                           .iteritems()):
            output_line = str(peer) + ' ' + str(primary_node) + ' ' + str(
                kAs1PeerAs2) + ' ' + str(peer_link_cost) + ' ' + str(
                    peer) + ' ' + str(primary_node) + '\n'
            open_file_handle.write(output_line)
        #remove last character in file, as it is guranteed to be a newline, which we don't want
    open_file_handle.seek(-1, os.SEEK_END)
    open_file_handle.truncate()


def CreateTier1Picker(args):
    """Factory method that creates a tier1 picker based on the 'picker' commandline argument

    Arguments:
       args: The argparse arguments from the commandline

    Returns: A Tier1Picker based on the 'picker' commandline argument
    """
    if args.picker == kDoStraightPercentage:
        return PercentageTier1Picker(
            int(args.seed), float(args.percentage_tier1s))
    if args.picker == kDoRawNumber:
        if args.starting_pt != None:
            return RawTier1Picker(int(args.seed), int(args.raw), int(args.starting_pt))
        else:
            return RawTier1Picker(int(args.seed), int(args.raw), None)

def LogTier1sAndNumberOfDescendents(caida_open_file_handle):
    """Logs some information about the tier1s.

    Arguments:
       caida_open_file_handle: The handle to the open caida file to form an
       adjacencylist out of
    """

    adjacencylist = utility_functions.ConvertToAdjacencyList(caida_open_file_handle)
    tier1s = utility_functions.GetTierOnes(adjacencylist)
    tier1s_to_descendents = {}
    for tier1_tuple in tier1s:
        tier1s_to_descendents[tier1_tuple[0]] = utility_functions.GetDescendents(tier1_tuple[0], adjacencylist, 0 )
    for tier1 in sorted(tier1s_to_descendents, key=tier1s_to_descendents.get):
        # log tier 1s and number of descendnets sorted by value for debugging sake
        logging.debug("tier1s to descendnets: %i, %i", tier1, len(tier1s_to_descendents[tier1]))
    caida_open_file_handle.seek(0)



def main():
    #get arguments
    args = InitializeArgParser()

    #open caida file
    caida_open_file_handle = open(args.input_converted_caida, 'r')

    #log some inforamtion
    LogTier1sAndNumberOfDescendents(caida_open_file_handle)

    #open write location
    write_location_file_handle = open(args.output_file, 'w+')

    #Create vertical slice
    caida_vertical_slice = CaidaVerticalSlice(caida_open_file_handle)
    # get picker
    tier1_picker = CreateTier1Picker(args)
    vertical_slice_graph = caida_vertical_slice.GetVerticalSlice(tier1_picker, float(args.depth), float(args.prune_factor))

    # generate link costs
    #restet seed
    random.seed(int(args.seed))
    uniform_random_link_cost_generator = UniformRandomLinkCostGenerator(10,
                                                                        1000)
    utility_functions.AssignRandomLinkCosts(uniform_random_link_cost_generator,
                                            vertical_slice_graph)

    #write vertical slice to file
    WriteGraphToFile(vertical_slice_graph, write_location_file_handle)


if __name__ == "__main__":
    logging.basicConfig(
        filename='/tmp/caida_vertical_slice.log', level=logging.DEBUG)
    main()
