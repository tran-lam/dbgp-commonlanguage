import utility_functions
from tier1_picker import Tier1Picker
import python_constants
from python_constants import kPeerDict
from python_constants import kProviderDict
from python_constants import kCustomerDict
import random
import logging


class CaidaVerticalSlice():
    """Class that creates a vertical slice of a Caida file."""
    # percent_tier_1s_ = 0
    # rgenerator_seed_ = 1
    open_caida_file_ = None

    def __init__(self, open_caida_file):
        self.open_caida_file_ = open_caida_file

    def GetVerticalSlice(self, tier1_picker, maxdepth=float("inf"), pruning_factor=float(0)):
        """Function that returns a subgraph of the total caida graph of the descedents
of a subset of tier1s as picked by 'tier1_picker. It operates based on the
passed in parameters of class construction.

        Arguments:
           tier1_picker: Implementation of abstract class 'Tier1Picker'. Used
           for picking tier1s.
           maxdepth: The maximum depth the vertical slice should extend to.
           Defualt is infinity.
           pruning_factor: the number of nodes to prune for each descendent
           level. Default is 0.

        Returns: A graph consisting of a vertical slice of the descedents of
        tier1s picked.

        """
        #constant for the tier1 position in the list of tuples retunred by
        #'utility_functions.GetTierOnes'
        kTier1PositionInTuple = 0

        return_slice_dictionary = {}

        #get adjacencylist
        full_caida_adjacency_list = utility_functions.ConvertToAdjacencyList(
            self.open_caida_file_)

        #get tier1s from adjacencylist. Remember this is a tuple of the tier1
        #and its neighbor tier1s (if it has any).
        tier1_list = utility_functions.GetTierOnes(full_caida_adjacency_list)
        logging.debug('Number of tier1s in dataset: %i', len(tier1_list))
        logging.debug('All tier1s: %s', tier1_list)

        non_tupled_tier1s_for_vertical_slice = tier1_picker.GetTier1sForVerticalSlice(
            tier1_list)

        logging.debug('NonTupled tier1s in slice : %s',
                      non_tupled_tier1s_for_vertical_slice)

        # create dictionary of tier1 ases to their descendents
        # and
        # create a dictionary of all ases that are a descendent of a tier1.
        # This is used when checking if a peer from the descendent of a tier1
        # is a descendent of another tier1
        tier1s_to_descendents = {}
        all_descendents = {}
        for tier1 in non_tupled_tier1s_for_vertical_slice:
            working_tier1 = tier1
            working_tier1_descendents = utility_functions.GetDescendents(
                working_tier1, full_caida_adjacency_list, pruning_factor)
            tier1s_to_descendents[working_tier1] = working_tier1_descendents
            all_descendents.update(working_tier1_descendents)


        # Get the min depth for all the descendents in the vertical slice, this
        # will be used in determining whether to actually put a descendent in
        # the returnslice dictionary.
        as_to_mindepth = self.GetAsNumToMinDepth(tier1s_to_descendents,
                                                 full_caida_adjacency_list)

        # Go through each descendent adjaceny list of a tier1s and add them to
        # the slice disregarding the peers of the descendents if their peers
        # are not a descendent of another tier1
        for tier1, tier1_descendents in tier1s_to_descendents.iteritems():

            # since tier1 is at depth 0, then it will always go onto the slice
            tier1_neighbors = full_caida_adjacency_list[tier1]
            return_slice_dictionary[tier1] = self.GetCorrectNeighbors(
                tier1_neighbors, as_to_mindepth, maxdepth)

            #now go through each of its descendents and add them if they have a
            #depth less than or equal to max depth
            for descendent_as, descendent_neighbors in tier1_descendents.iteritems(
            ):
                if(as_to_mindepth[descendent_as] <= maxdepth):
                    return_slice_dictionary[
                        descendent_as] = self.GetCorrectNeighbors(
                            descendent_neighbors, as_to_mindepth, maxdepth)

        return return_slice_dictionary

    #private methods below here

    def GetCorrectNeighbors(self, neighbor_dicts, as_to_min_depth, max_depth):
        """Function that returns the correct neighbordictionary ready to be placed into
the vertical slice to be written to a file. Used in 'GetVerticalSlice'

        Arguments:
           neighbor_dicts: The dictionaries to clean up. The neighbors of some AS.
           as_to_min_depth: Dictionary containing the min depth of all descendents
           max_depth: the maximum depth of ases

        Returns: A list of dictionararies that are ready to be written to the
        slice to return
        """

        correct_neighbors_for_slice = []

        # for each set of neighbors, if each AS in them is not in
        # as_to_min_depth, disregard it (because if an AS is not in
        # as_to_min_depth, then it is not a descendent of a tier1 that we are
        # taking the vertical slice from). If it is, then the depth associated
        # with it must be less than or equal to max_depth.
        for neighbor_dict in neighbor_dicts:
            dict_to_append_to_correct_neighbors = {}
            for asnum in neighbor_dict:
                if asnum in as_to_min_depth:
                    if as_to_min_depth[asnum] <= max_depth:
                        dict_to_append_to_correct_neighbors[asnum] = -1
            correct_neighbors_for_slice.append(
                dict_to_append_to_correct_neighbors)
        return correct_neighbors_for_slice

    def GetAsNumToMinDepth(self, tier1_to_descendents, full_graph):
        """Function that gets the min depth for each as in the descendents of tier1s in
the vertical slice.

        Arguments:
           tier1_to_descendents: A dictionary where the key is a tier1 and the
        value is a adjacency list of its descendents.
           full_graph: The full adjacency list. Used for getting the first
           descendents of a tier1

        Returns: A dictionary where the key is an AS, and the value is the min
        depth of the AS.
        """

        return_as_to_mindepth = {}

        # for each tier1 and its descendents, perform breadth first search to
        # get the depth of each descendent. Add to return dictionary if the
        # node isn't in there or the node to be added has a lower min depth
        # than the entry in the dictionary.
        for tier1, descendents in tier1_to_descendents.iteritems():
            descendent_depth_dict = self.GetDescendentDepth(tier1, descendents,
                                                            full_graph)
            #for each AS and depth, only add to return dict if the depth here
            #is less than the depth there already (if there is one)
            for asnum, depth in descendent_depth_dict.iteritems():
                if asnum not in return_as_to_mindepth:
                    return_as_to_mindepth[asnum] = depth
                else:
                    if return_as_to_mindepth[asnum] > depth:
                        return_as_to_mindepth[asnum] = depth
        return return_as_to_mindepth

    def GetDescendentDepth(self, tier1, descendents, full_graph):
        """Function that returns a min depth dictionary for the descendents of a single tier1.

        Arguments:
           tier1: the tier1 who 'descendents' are associated with. descendents:
        the adjacency list of the descendents of 'tier1'
           full_graph: the full adjacency list

        Returns: A asnum to mindepth of the descendents for this particular tier1.

        """
        # holds the information of the depth of nodes. Initialize with tier1
        # being at level 0.
        as_to_depth_dict = {tier1 : 0}
        # The queue of nodes to search next in BFS.
        descendents_to_look_at = []
        #add the first descendents to look at
        descendents_to_look_at += list(full_graph[tier1][kCustomerDict].keys())

        #this first round of descendents depth is 1, so put that in the dict
        for descendent in descendents_to_look_at:
            as_to_depth_dict[descendent] = 1

        while len(descendents_to_look_at) > 0:
            working_node = descendents_to_look_at.pop(0)
            # get working_node descendents
            working_node_descendents = full_graph[working_node][
                kCustomerDict].keys()
            for working_node_descendent in working_node_descendents:
                if working_node_descendent not in as_to_depth_dict:
                    as_to_depth_dict[
                        working_node_descendent] = as_to_depth_dict[
                            working_node] + 1
                    descendents_to_look_at.append(working_node_descendent)
        return as_to_depth_dict

    def GetValidPeersInVerticalSlice(self, as_dict, all_descendents,
                                     tier1s_in_vertical_slice):
        """Function that returns a dictionary of asnums that are either a tier1 in the
vertical slice or a descendent of a tier1 in the vertical slice

        Arguments:
           as_dict: The dictionary of ASes to prune to only ASes that are
           either a tier1 in the vertical slice or a descendent of a tier1
           all_descendents: lookuptable containing all descendnets of tier1s in vertical slice
           tier1s_in_vertical_slice: The tier 1s that are apart of the slice we are taking

        Returns: A dictionary of ASes that are either descendents of a tier1 in
        the vertical slice or a descendent of a tier1 in the vertical slice.

        """
        return_peer_dict = {}
        for as_num in as_dict:
            if as_num in all_descendents or as_num in tier1s_in_vertical_slice:
                return_peer_dict[as_num] = as_dict[as_num]
        return return_peer_dict
