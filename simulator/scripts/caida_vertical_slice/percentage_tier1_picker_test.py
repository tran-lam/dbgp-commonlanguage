import unittest
from percentage_tier1_picker import PercentageTier1Picker

class PercentageTier1PickerTest(unittest.TestCase):
    kInputTier1List = [
        (1, []),
        (2, []),
        (3, []),
        (4, []),
        (5, []),
        (6, []),
        (7, []),
        (8, []),
        (9, []),
        (10, []),
        (11, []),
        (12, []),
        (13, []),
        (14, []),
        (15, []),
        (16, []),
        (17, []),
        (18, []),
        (19, []),
        (20, []),
        (21, []),
        (22, []),
        (23, []),
        (24, []),
        (25, []),
        (26, []),
        (27, []),
        (28, []),
        (29, []),
        (30, []),
        (31, []),
        (32, []),
        (33, []),
        (34, []),
        (35, []),
        (36, []),
        (37, []),
        (38, []),
        (39, []),
        (40, []),
        (41, []),
        (42, []),
        (43, []),
        (44, []),
        (45, []),
        (46, []),
        (47, []),
        (48, []),
        (49, []),
        (50, [])
    ]

    def testXGivenListOfTier1sAnd0PercentXGetListSize0BAck(self):
        #arrange
        kPercentage = 0

        kCorrectListSize = 0

        percentage_tier1_picker = PercentageTier1Picker(1, kPercentage)

        #act
        result_list = percentage_tier1_picker.GetTier1sForVerticalSlice(self.kInputTier1List)

        #assert
        self.assertTrue(len(result_list) == kCorrectListSize)

    def testXGivenListOfTier1sAnd51Pt3PercentXGetListSize26BAck(self):
        #arrange
        kPercentage = .513

        kCorrectListSize = 26

        percentage_tier1_picker = PercentageTier1Picker(1, kPercentage)

        #act
        result_list = percentage_tier1_picker.GetTier1sForVerticalSlice(self.kInputTier1List)

        #assert
        self.assertTrue(len(result_list) == kCorrectListSize)

    def testXGivenListOfTier1sAnd100PercentXGetListSize50BAck(self):
        #arrange
        kPercentage = 1

        kCorrectListSize = 50

        percentage_tier1_picker = PercentageTier1Picker(1, kPercentage)

        #act
        result_list = percentage_tier1_picker.GetTier1sForVerticalSlice(self.kInputTier1List)

        #assert
        self.assertTrue(len(result_list) == kCorrectListSize)


suite = unittest.TestLoader().loadTestsFromTestCase(PercentageTier1PickerTest)
runner = unittest.TextTestRunner()
runner.run(suite)
