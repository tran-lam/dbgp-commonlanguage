import unittest
from raw_tier1_picker import RawTier1Picker


class RawTier1PickerTest(unittest.TestCase):

    def testXGetTier1sForVerticalSliceXPick1From1XGet1Back(self):
        #arrange
        kInputTuples = [(1, [])]
        kNumToPick = 1
        kRgeneratorSeed = 1

        kNumToGetBack = 1

        raw_tier1_picker = RawTier1Picker(kRgeneratorSeed, kNumToPick)
        #act
        tier1_list = raw_tier1_picker.GetTier1sForVerticalSlice(kInputTuples)

        #assert
        self.assertEqual(len(tier1_list), kNumToGetBack)

    def testXGetTier1sForVerticalSliceXPick3From6XGet3Back(self):
        #arrange
        kInputTuples = [(1, [2]),
                        (2, [3,1]),
                        (3, [2,4]),
                        (4, [3,5]),
                        (5, [4,6]),
                        (6, [5]),
        ]
        kNumToPick = 3
        kRgeneratorSeed = 1

        kNumToGetBack = 3

        raw_tier1_picker = RawTier1Picker(kRgeneratorSeed, kNumToPick)
        #act
        tier1_list = raw_tier1_picker.GetTier1sForVerticalSlice(kInputTuples)


        #assert
        self.assertEqual(len(tier1_list), kNumToGetBack)


    def testXGetTier1sForVerticalSliceXPick4From6XGetEmptyBack(self):
        #arrange
        kInputTuples = [(1, [2]),
                        (2, [3,1]),
                        (3, [2]),
                        (4, [5]),
                        (5, [4,6]),
                        (6, [5]),
        ]
        kNumToPick = 4
        kRgeneratorSeed = 1

        kNumToGetBack = 0

        raw_tier1_picker = RawTier1Picker(kRgeneratorSeed, kNumToPick)
        #act
        tier1_list = raw_tier1_picker.GetTier1sForVerticalSlice(kInputTuples)


        #assert
        self.assertEqual(len(tier1_list), kNumToGetBack)

    def testXGetTier1sForVerticalSliceXSpecificPickNonExistentXGetEmptyBack(self):
        #arrange
        kInputTuples = [(1, [2])
        ]
        kNumToPick = 1
        kRgeneratorSeed = 1
        kStartingPt = 5

        kNumToGetBack = 0

        raw_tier1_picker = RawTier1Picker(kRgeneratorSeed, kNumToPick, kStartingPt)
        #act
        tier1_list = raw_tier1_picker.GetTier1sForVerticalSlice(kInputTuples)

        #assert
        self.assertEqual(len(tier1_list), kNumToGetBack)

    def testXGetTier1sForVerticalSliceXSpecificPickExistentXGetEmptyBack(self):
        #arrange
        kInputTuples = [(1, [2])
        ]
        kNumToPick = 1
        kRgeneratorSeed = 1
        kStartingPt = 1

        kNumToGetBack = 1

        raw_tier1_picker = RawTier1Picker(kRgeneratorSeed, kNumToPick, kStartingPt)
        #act
        tier1_list = raw_tier1_picker.GetTier1sForVerticalSlice(kInputTuples)
        print tier1_list

        #assert
        self.assertEqual(len(tier1_list), kNumToGetBack)

    def testXGetTier1sForVerticalSliceXSpecificPick1Get1BackXGetEmptyBack(self):
        #arrange
        kInputTuples = [(1, [2]),
                        (2, [1,3]),
                        (3, [2]),
        ]
        kNumToPick = 1
        kRgeneratorSeed = 1
        kStartingPt = 1

        kNumToGetBack = 1

        raw_tier1_picker = RawTier1Picker(kRgeneratorSeed, kNumToPick, kStartingPt)
        #act
        tier1_list = raw_tier1_picker.GetTier1sForVerticalSlice(kInputTuples)

        #assert
        self.assertEqual(len(tier1_list), kNumToGetBack)
        self.assertEqual(tier1_list, [1])


suite = unittest.TestLoader().loadTestsFromTestCase(RawTier1PickerTest)
runner = unittest.TextTestRunner()
runner.run(suite)
