

class LinkCostGenerator():
    """Abstract class for assigning random costs to links.

    Example in a situation where this might be used:
    ,----
    | my_link_cost_generator = CustomLinkCostGenerator()
    | def AFunctionThatUsesBaseClass(my_link_cost_generator):
    |     my_link_cost_generator.GenerateLinkCost()
    `----
    """

    def GenerateLinkCost():
        """Abstract method for generating a link cost.

        Returns: A float value that will be assigned to a link.
        """
        raise NotImplementedError("Subclass must implement abstract method")




