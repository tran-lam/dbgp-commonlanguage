
class Tier1Picker():
    """Abstract class for choosing tier 1s to get a vertical slice from. This is so
that we can make choosing on different methods easy to be created.
    """

    def GetTier1sForVerticalSlice(self, tier1_tuples):
        """Returns a list of tier1s to use in a vertical slice.

        Arguments:
           tier1_tuples: A list of tier 1s in format (<tier1>, [<neighbors that
        are tier1> ...]).  This is generated from 'utility_functions.GetTierOnes'

        Returns: A plain list of tier 1s.

        """
        raise NotImplementedError("Subclass must implement abstract method")
