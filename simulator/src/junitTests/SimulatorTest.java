package junitTests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simulator.Simulator;

public class SimulatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetPassThroughAses_GivenSetOfAsNumsWith50Percent_ReturnSetOfSize5() {
      // Arrange
      Set<Integer> kSampleSet = new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
      double kSamplePercent =.5;

      // Act
      HashSet<Integer> result_set = Simulator.GetPassThroughAses(kSampleSet, kSamplePercent);

      // Assert
      assertEquals(result_set.size(), 5);


		// fail("Not yet implemented"); // TODO
	}
	
	@Test
	public void testGetPassThroughAses_GivenSetOfAsNumsWith90Percent_ReturnSetOfSize9() {
      // Arrange
      Set<Integer> kSampleSet = new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
      double kSamplePercent =.9;

      // Act
      HashSet<Integer> result_set = Simulator.GetPassThroughAses(kSampleSet, kSamplePercent);

      // Assert
      assertEquals(result_set.size(), 9);

	}
	
	@Test
	public void testGetPassThroughAses_GivenSetOfAsNumsWith91Percent_ReturnSetOfSize10() {
      // Arrange
      Set<Integer> kSampleSet = new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
      double kSamplePercent =.91;

      // Act
      HashSet<Integer> result_set = Simulator.GetPassThroughAses(kSampleSet, kSamplePercent);

      // Assert
      assertEquals(result_set.size(), 10);

	}
	
	@Test
	public void testGetPassThroughAses_GivenSetOfAsNumsWith100Percent_ReturnSetOfSize10() {
      // Arrange
      Set<Integer> kSampleSet = new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
      double kSamplePercent =1;

      // Act
      HashSet<Integer> result_set = Simulator.GetPassThroughAses(kSampleSet, kSamplePercent);

      // Assert
      assertEquals(result_set.size(), 10);

	}
	
	@Test
	public void testGetPassThroughAses_GivenSetOfAsNumsWith0Percent_ReturnSetOfSize0() {
      // Arrange
      Set<Integer> kSampleSet = new HashSet<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
      double kSamplePercent =0;

      // Act
      HashSet<Integer> result_set = Simulator.GetPassThroughAses(kSampleSet, kSamplePercent);

      // Assert
      assertEquals(result_set.size(), 0);

	}


}
